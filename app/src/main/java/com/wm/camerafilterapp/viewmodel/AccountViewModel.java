package com.wm.camerafilterapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


;import com.wm.camerafilterapp.model.changepasswordmodel.ChangepasswordResponse;
import com.wm.camerafilterapp.model.resetpasswordmodel.ResetPasswordResponse;
import com.wm.camerafilterapp.model.forgotpasswordmodel.ForgotpasswordResponse;
import com.wm.camerafilterapp.model.loginmodel.LoginResponse;
import com.wm.camerafilterapp.model.registermodel.RegisterResponse;
import com.wm.camerafilterapp.model.verificationmodel.VerificationResponse;
import com.wm.camerafilterapp.repositories.AuthenticationRepositories;


public class AccountViewModel extends AndroidViewModel
{
    private AuthenticationRepositories accountRepositories;
    public AccountViewModel(@NonNull Application application)
    {
        super(application);
        accountRepositories = new AuthenticationRepositories();
    }

    public LiveData<RegisterResponse> RegisterUser(String email,
                                                   String mobileno,
                                                   String password,
                                                   String type,
                                                   String devicetoken)
    {
        return accountRepositories.RegisterUser(email,mobileno,password,
                                                type,devicetoken);
    }

    public LiveData<LoginResponse> LoginUser(String email,
                                             String password,
                                             String type,
                                             String devicetoken)
    {
        return accountRepositories.LoginUser(email,password,
                type,devicetoken);
    }

    public LiveData<ForgotpasswordResponse> ForgotPassword(String email)
    {
        return accountRepositories.ForgotPassword(email);
    }


    public LiveData<VerificationResponse> VerificationOtp(String email, String otp)
    {
        return accountRepositories.VerificationOtp(email,otp);
    }

    public LiveData<ResetPasswordResponse> resetpassword(String email, String newpassword, String confirm_password)
    {
        return accountRepositories.resetpassword(email,newpassword,confirm_password);
    }

    public LiveData<ChangepasswordResponse> ChangePassword(String token,String email,String oldpassword, String newpassword, String confirm_password)
    {
        return accountRepositories.changepassword(token,email,oldpassword,newpassword,confirm_password);
    }
}
