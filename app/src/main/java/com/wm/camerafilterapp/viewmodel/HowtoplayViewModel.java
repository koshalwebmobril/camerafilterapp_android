package com.wm.camerafilterapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.wm.camerafilterapp.model.howtoplaymodel.HowtoPlayResponse;
import com.wm.camerafilterapp.repositories.HowtoplayRepositories;


public class HowtoplayViewModel extends AndroidViewModel
{
    private HowtoplayRepositories howtoplayRepositories;
    public HowtoplayViewModel(@NonNull Application application)
    {
        super(application);
        howtoplayRepositories = new HowtoplayRepositories();
    }

    public LiveData<HowtoPlayResponse> Howtoplay(String token)
    {
        return howtoplayRepositories.Howtoplay(token);
    }


}
