package com.wm.camerafilterapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.wm.camerafilterapp.model.postReportProblemModel.PostReportProblemResponse;
import com.wm.camerafilterapp.repositories.ReportProblemRepositories;


public class ReportProblemViewModel extends AndroidViewModel
{
    private ReportProblemRepositories reportProblemRepositories;
    public ReportProblemViewModel(@NonNull Application application)
    {
        super(application);
        reportProblemRepositories = new ReportProblemRepositories();
    }

    public LiveData<PostReportProblemResponse> PostReport(String userId,String selectedissueId, String comment)
    {
        return reportProblemRepositories.postReportProblem(userId,selectedissueId,comment);
    }


}
