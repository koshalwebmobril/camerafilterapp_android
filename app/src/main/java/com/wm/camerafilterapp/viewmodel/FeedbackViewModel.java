package com.wm.camerafilterapp.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.wm.camerafilterapp.model.feedbackmodel.FeedbackResponse;
import com.wm.camerafilterapp.repositories.FeedbackRepositories;


public class FeedbackViewModel extends AndroidViewModel
{
    private FeedbackRepositories feedbackRepositories;
    public FeedbackViewModel(@NonNull Application application)
    {
        super(application);
        feedbackRepositories = new FeedbackRepositories();
    }

    public LiveData<FeedbackResponse> Feedbackform(String token,String message,String rating)
    {
        return feedbackRepositories.Feedbackform(token,message,rating);
    }


}
