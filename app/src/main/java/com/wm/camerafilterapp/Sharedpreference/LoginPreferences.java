package com.wm.camerafilterapp.Sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class LoginPreferences
{
    private static com.wm.camerafilterapp.Sharedpreference.LoginPreferences preferences = null;
    private static SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private String stateId;
    private Context context;
    private String token = "token";
    private  String company_email="email";
    private  String company_name="name";
    private  String profile_image="profile_image";
    private  String user_id="user_id";

    public LoginPreferences(Context context) {
        this.context = context;
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }

    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    private void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }

    public static com.wm.camerafilterapp.Sharedpreference.LoginPreferences getActiveInstance(Context context)
    {
        if (preferences == null) {
            preferences = new com.wm.camerafilterapp.Sharedpreference.LoginPreferences(context);
        }
        return preferences;
    }

    public String getToken()
    {
        return mPreferences.getString(this.token, "");
    }

    public void setToken(String token)
    {
        editor = mPreferences.edit();
        editor.putString(this.token, "Bearer "+ token);
        editor.apply();
    }


    public String getUserId()
    {
        return mPreferences.getString(this.user_id, "");
    }

    public void setUserId(String user_id)
    {
        editor = mPreferences.edit();
        editor.putString(this.user_id, user_id);
        editor.apply();
    }

    public void setUserName(String name)
    {
        editor = mPreferences.edit();
        editor.putString(this.company_name, name);
        editor.apply();
    }

    public String getUserName()
    {
        return mPreferences.getString(this.company_name, "");
    }



    public void setUserEmail(String email)
    {
        editor = mPreferences.edit();
        editor.putString(this.company_email, email);
        editor.apply();
    }

    public String getUserEmail()
    {
        return mPreferences.getString(this.company_email, "");
    }

    public static void deleteAllPreference()
    {
        mPreferences.edit().clear().apply();
    }


    public String getUserProfile()
    {
        return mPreferences.getString(this.profile_image, "");
    }
    public void setUserProfile(String profile_image)
    {
        editor = mPreferences.edit();
        editor.putString(this.profile_image, profile_image);
        editor.apply();
    }
}
