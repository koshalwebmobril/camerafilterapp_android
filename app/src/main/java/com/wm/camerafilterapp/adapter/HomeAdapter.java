package com.wm.camerafilterapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.interfase.CameraFilterListner;
import com.wm.camerafilterapp.model.filterimagenamemodel.FilterImageModel;

import java.util.ArrayList;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {
    ArrayList<FilterImageModel> filterimagelist;
    Context context;
    CameraFilterListner cameraFilterListner;

    public HomeAdapter(Context context, ArrayList filterimagelist, CameraFilterListner cameraFilterListner) {
        this.context = context;
        this.filterimagelist = filterimagelist;
        this.cameraFilterListner = cameraFilterListner;
    }

    @Override
    public HomeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false);
        HomeAdapter.MyViewHolder vh = new HomeAdapter.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final HomeAdapter.MyViewHolder holder, final int position) {
        final FilterImageModel filterImageModel = filterimagelist.get(position);

        holder.filter_name.setText(filterImageModel.getName());
        holder.filter_image.setImageResource(filterImageModel.getImage());

        if (filterImageModel.getStatus() == 0)
            holder.filter_name.setTextColor(ContextCompat.getColor(context, R.color.unselected_bg_color));
        else
            holder.filter_name.setTextColor(ContextCompat.getColor(context, R.color.selected_bg_color));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterImageModel.getStatus() == 0)
                {
                    for (int i = 0; i < filterimagelist.size(); i++)
                    {
                        if (i == position)
                            filterimagelist.get(i).setStatus(1);
                        else
                            filterimagelist.get(i).setStatus(0);
                    }

                    cameraFilterListner.onSelectFilter(filterimagelist.get(position).getId(),position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filterimagelist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView filter_image;
        TextView filter_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            filter_image = itemView.findViewById(R.id.filter_image);
            filter_name = itemView.findViewById(R.id.filter_name);
        }
    }
}