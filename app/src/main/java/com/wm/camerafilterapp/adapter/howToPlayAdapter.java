package com.wm.camerafilterapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.model.howtoplaymodel.ResultItem;

import java.util.List;

public class howToPlayAdapter extends RecyclerView.Adapter<howToPlayAdapter.MyViewHolder>
{
    Context context;
    List<ResultItem> resultItemList;
    public howToPlayAdapter(Context context,List resultItemList)
    {
        this.context = context;
        this.resultItemList=resultItemList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_how_to_play, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position)
    {
         context = holder.itemView.getContext();
         ResultItem reviewlist = resultItemList.get(position);
         holder.title_text.setText(reviewlist.getTitle());
         holder.description_text.setText(reviewlist.getDescription());
         holder.rules_count.setText(String.valueOf(position+1));
    }
    @Override
    public int getItemCount()
    {
        return resultItemList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView title_text,description_text,rules_count;
        public MyViewHolder(View itemView)
        {
            super(itemView);
            title_text=itemView.findViewById(R.id.title_text);
            description_text=itemView.findViewById(R.id.description_text);
            rules_count=itemView.findViewById(R.id.rules_count);
        }
    }
}