package com.wm.camerafilterapp.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.wm.camerafilterapp.model.postReportProblemModel.PostReportProblemResponse;
import com.wm.camerafilterapp.network.ApiInterface;
import com.wm.camerafilterapp.network.RetrofitConnection;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportProblemRepositories
{ public LiveData<PostReportProblemResponse> postReportProblem(String user_id, String reason_id, String comment)
    {
        MutableLiveData<PostReportProblemResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<PostReportProblemResponse> call = apiService.postreportproblem(user_id,reason_id,comment);
        call.enqueue(new Callback<PostReportProblemResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<PostReportProblemResponse> call, @NonNull Response<PostReportProblemResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                    {
                        PostReportProblemResponse statusMessageModel=new PostReportProblemResponse();
                        statusMessageModel.isError();
                        mutableLiveData.setValue(statusMessageModel);
                    }
            }
            @Override
            public void onFailure(@NonNull Call<PostReportProblemResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                PostReportProblemResponse statusMessageModel=new PostReportProblemResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }
}
