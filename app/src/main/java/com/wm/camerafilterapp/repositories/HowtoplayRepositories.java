package com.wm.camerafilterapp.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.wm.camerafilterapp.model.howtoplaymodel.HowtoPlayResponse;
import com.wm.camerafilterapp.network.ApiInterface;
import com.wm.camerafilterapp.network.RetrofitConnection;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HowtoplayRepositories
{
    private String TAG= HowtoplayRepositories.class.getSimpleName();
    public LiveData<HowtoPlayResponse> Howtoplay(String token)
    {
        MutableLiveData<HowtoPlayResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<HowtoPlayResponse> call = apiService.Howtoplay(token);
        call.enqueue(new Callback<HowtoPlayResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<HowtoPlayResponse> call, @NonNull Response<HowtoPlayResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                    {
                        HowtoPlayResponse statusMessageModel=new HowtoPlayResponse();
                        statusMessageModel.isError();
                        mutableLiveData.setValue(statusMessageModel);
                    }
            }
            @Override
            public void onFailure(@NonNull Call<HowtoPlayResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                Log.e(TAG, new Gson().toJson(t.getCause()));
                HowtoPlayResponse statusMessageModel=new HowtoPlayResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }
}
