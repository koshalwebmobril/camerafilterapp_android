package com.wm.camerafilterapp.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;


import com.google.gson.Gson;
import com.wm.camerafilterapp.model.changepasswordmodel.ChangepasswordResponse;
import com.wm.camerafilterapp.model.resetpasswordmodel.ResetPasswordResponse;
import com.wm.camerafilterapp.model.forgotpasswordmodel.ForgotpasswordResponse;
import com.wm.camerafilterapp.model.loginmodel.LoginResponse;
import com.wm.camerafilterapp.model.registermodel.RegisterResponse;
import com.wm.camerafilterapp.model.verificationmodel.VerificationResponse;
import com.wm.camerafilterapp.network.ApiInterface;
import com.wm.camerafilterapp.network.RetrofitConnection;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthenticationRepositories
{
    private String TAG= com.wm.camerafilterapp.repositories.AuthenticationRepositories.class.getSimpleName();

    public LiveData<RegisterResponse> RegisterUser
            (String email, String mobileno, String password,String type,String devicetoken)
    {
        MutableLiveData<RegisterResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<RegisterResponse> call = apiService.RegisterUser(email,mobileno,password,type,devicetoken);
        call.enqueue(new Callback<RegisterResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<RegisterResponse> call, @NonNull Response<RegisterResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                    {
                    RegisterResponse statusMessageModel=new RegisterResponse();
                    statusMessageModel.isError();
                    mutableLiveData.setValue(statusMessageModel);
                  }
            }
            @Override
            public void onFailure(@NonNull Call<RegisterResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                Log.e(TAG, new Gson().toJson(t.getCause()));
                RegisterResponse statusMessageModel=new RegisterResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }


    public LiveData<LoginResponse> LoginUser(String email,String password,String type,String devicetoken)
    {
        MutableLiveData<LoginResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<LoginResponse> call = apiService.LoginUser(email,password,type,devicetoken);
        call.enqueue(new Callback<LoginResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                {
                    LoginResponse statusMessageModel=new LoginResponse();
                    statusMessageModel.isError();
                    mutableLiveData.setValue(statusMessageModel);
                }
            }
            @Override
            public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                Log.e(TAG, new Gson().toJson(t.getCause()));
                LoginResponse statusMessageModel=new LoginResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }


    public LiveData<ForgotpasswordResponse> ForgotPassword(String email)
    {
        MutableLiveData<ForgotpasswordResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<ForgotpasswordResponse> call = apiService.Forgotpassword(email);
        call.enqueue(new Callback<ForgotpasswordResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<ForgotpasswordResponse> call, @NonNull Response<ForgotpasswordResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                {
                    ForgotpasswordResponse statusMessageModel=new ForgotpasswordResponse();
                    statusMessageModel.isError();
                    mutableLiveData.setValue(statusMessageModel);
                }
            }
            @Override
            public void onFailure(@NonNull Call<ForgotpasswordResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                Log.e(TAG, new Gson().toJson(t.getCause()));
                ForgotpasswordResponse statusMessageModel=new ForgotpasswordResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }


    public LiveData<VerificationResponse> VerificationOtp(String email, String otp)
    {
        MutableLiveData<VerificationResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<VerificationResponse> call = apiService.verifiyotp(email,otp);
        call.enqueue(new Callback<VerificationResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<VerificationResponse> call, @NonNull Response<VerificationResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                {
                    VerificationResponse statusMessageModel=new VerificationResponse();
                    statusMessageModel.isError();
                    mutableLiveData.setValue(statusMessageModel);
                }
            }
            @Override
            public void onFailure(@NonNull Call<VerificationResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                Log.e(TAG, new Gson().toJson(t.getCause()));
                VerificationResponse statusMessageModel=new VerificationResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }


    public LiveData<ResetPasswordResponse> resetpassword(String email, String newpassword, String confirmpassword)
    {
        MutableLiveData<ResetPasswordResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<ResetPasswordResponse> call = apiService.ResetPassword(email,newpassword,confirmpassword);
        call.enqueue(new Callback<ResetPasswordResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<ResetPasswordResponse> call, @NonNull Response<ResetPasswordResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                {
                    ResetPasswordResponse statusMessageModel=new ResetPasswordResponse();
                    statusMessageModel.isError();
                    mutableLiveData.setValue(statusMessageModel);
                }
            }
            @Override
            public void onFailure(@NonNull Call<ResetPasswordResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                Log.e(TAG, new Gson().toJson(t.getCause()));
                ResetPasswordResponse statusMessageModel=new ResetPasswordResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }


    public LiveData<ChangepasswordResponse> changepassword(String token,String email,String oldpassword, String newpassword, String confirmpassword)
    {
        MutableLiveData<ChangepasswordResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<ChangepasswordResponse> call = apiService.changepassword(token,
                 email,oldpassword,newpassword,confirmpassword);
        call.enqueue(new Callback<ChangepasswordResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<ChangepasswordResponse> call, @NonNull Response<ChangepasswordResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                {
                    ChangepasswordResponse statusMessageModel=new ChangepasswordResponse();
                    statusMessageModel.isError();
                    mutableLiveData.setValue(statusMessageModel);
                }
            }
            @Override
            public void onFailure(@NonNull Call<ChangepasswordResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                Log.e(TAG, new Gson().toJson(t.getCause()));
                ChangepasswordResponse statusMessageModel=new ChangepasswordResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }
}
