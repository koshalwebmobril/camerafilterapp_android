package com.wm.camerafilterapp.repositories;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.wm.camerafilterapp.model.feedbackmodel.FeedbackResponse;
import com.wm.camerafilterapp.network.ApiInterface;
import com.wm.camerafilterapp.network.RetrofitConnection;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackRepositories
{
    public LiveData<FeedbackResponse> Feedbackform(String token,String message,String rating)
    {
        MutableLiveData<FeedbackResponse> mutableLiveData = new MutableLiveData<>();
        ApiInterface apiService = RetrofitConnection.getInstance().createService();
        Call<FeedbackResponse> call = apiService.feedbackform(token,message,rating);
        call.enqueue(new Callback<FeedbackResponse>()
        {
            @Override
            public void onResponse(@NonNull Call<FeedbackResponse> call, @NonNull Response<FeedbackResponse> response) {
                if (response.isSuccessful())
                {
                    mutableLiveData.setValue(response.body());
                }
                else
                    {
                        FeedbackResponse statusMessageModel=new FeedbackResponse();
                        statusMessageModel.isError();
                        mutableLiveData.setValue(statusMessageModel);
                    }
            }
            @Override
            public void onFailure(@NonNull Call<FeedbackResponse> call, @NonNull Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
                FeedbackResponse statusMessageModel=new FeedbackResponse();
                statusMessageModel.isError();
                mutableLiveData.setValue(statusMessageModel);
            }
        });
        return mutableLiveData;
    }
}
