package com.wm.camerafilterapp.network;

import com.wm.camerafilterapp.model.changepasswordmodel.ChangepasswordResponse;
import com.wm.camerafilterapp.model.feedbackmodel.FeedbackResponse;
import com.wm.camerafilterapp.model.getReportProblemmodel.GetIssueListResponse;
import com.wm.camerafilterapp.model.howtoplaymodel.HowtoPlayResponse;
import com.wm.camerafilterapp.model.postReportProblemModel.PostReportProblemResponse;
import com.wm.camerafilterapp.model.resetpasswordmodel.ResetPasswordResponse;
import com.wm.camerafilterapp.model.forgotpasswordmodel.ForgotpasswordResponse;
import com.wm.camerafilterapp.model.loginmodel.LoginResponse;
import com.wm.camerafilterapp.model.registermodel.RegisterResponse;
import com.wm.camerafilterapp.model.verificationmodel.VerificationResponse;
import com.wm.camerafilterapp.utils.UrlApi;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface
{
    @FormUrlEncoded
    @POST(UrlApi.REGISTER)
    Call<RegisterResponse> RegisterUser(@Field("email") String email,
                                        @Field("mobile") String mobile,
                                        @Field("password") String password,
                                        @Field("device_type") String deviceType,
                                        @Field("device_token") String deviceToken);


    @FormUrlEncoded
    @POST(UrlApi.LOGIN)
    Call<LoginResponse> LoginUser(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("device_type") String deviceType,
                                  @Field("device_token") String deviceToken);



    @FormUrlEncoded
    @POST(UrlApi.FORGOTPASSWORD)
    Call<ForgotpasswordResponse> Forgotpassword(@Field("email") String email);


    @FormUrlEncoded
    @POST(UrlApi.VERIFYOTP)
    Call<VerificationResponse> verifiyotp(@Field("email") String email,
                                            @Field("otp") String otp);

    @FormUrlEncoded
    @POST(UrlApi.RESETPASSWORD)
    Call<ResetPasswordResponse> ResetPassword(@Field("email") String email, @Field("password") String password,
                                              @Field("password_confirmation") String confirm_password);


    @FormUrlEncoded
    @POST(UrlApi.CHANGEPASSWORD)
    Call<ChangepasswordResponse> changepassword(@Header("Authorization") String token,
                                                 @Field("email") String email,
                                                @Field("old_password") String old_password,
                                                @Field("password") String password,
                                                @Field("confirm_password") String confirm_password);


    @FormUrlEncoded
    @POST(UrlApi.FEEDBACKFORM)
    Call<FeedbackResponse> feedbackform(  @Header("Authorization") String token,
                                          @Field("message") String message,
                                          @Field("rating") String rating);

    @FormUrlEncoded
    @POST(UrlApi.REPORTAPROBLEMAPI)
    Call<PostReportProblemResponse> postreportproblem(@Field("user_id") String user_id,
                                                 @Field("reason_id") String reason_id,
                                                 @Field("comment") String comment);


    @GET(UrlApi.GAMERULE)
    Call<HowtoPlayResponse> Howtoplay(@Header("Authorization") String token);

    @GET(UrlApi.REASON)
    Call<GetIssueListResponse> GetIssueList();
}
