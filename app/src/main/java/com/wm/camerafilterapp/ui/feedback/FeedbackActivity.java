package com.wm.camerafilterapp.ui.feedback;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;


import com.wm.camerafilterapp.databinding.ActivityFeedbackBinding;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.FeedbackViewModel;

public class FeedbackActivity extends AppCompatActivity implements View.OnClickListener
{
     private ActivityFeedbackBinding binding;
     ProgressD progressDialog;
     private FeedbackViewModel feedbackViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);
        feedbackViewModel = new ViewModelProvider(this).get(FeedbackViewModel.class);
        binding.btnSubmit.setOnClickListener(this);
        binding.backIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                if(CommonMethod.isOnline(FeedbackActivity.this))
                {
                    hideKeyboard((Button)v);
                    if (validation())
                    {
                        ReviewApi(binding.reviewedittext.getText().toString().trim(),String.valueOf(binding.ratingbar.getRating()));
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), FeedbackActivity.this);
                }
                break;

            case R.id.back_icon:
                finish();
                break;
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(binding.reviewedittext.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Review", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private void ReviewApi(String feedback_text, String rating)
    {
        progressDialog = ProgressD.show(FeedbackActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        feedbackViewModel.Feedbackform(LoginPreferences.getActiveInstance(this).getToken(),feedback_text,rating).observe(this, FeedbackResponse ->
        {
            progressDialog.dismiss();
            if(FeedbackResponse !=null && FeedbackResponse.getCode()==200)
            {
                Toast.makeText(FeedbackActivity.this, FeedbackResponse.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
            else
            {
                Toast.makeText(FeedbackActivity.this, FeedbackResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}