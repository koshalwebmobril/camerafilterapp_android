package com.wm.camerafilterapp.ui.reportproblem;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;
import com.wm.camerafilterapp.databinding.ActivityReportProblemBinding;
import com.wm.camerafilterapp.model.getReportProblemmodel.GetIssueListResponse;
import com.wm.camerafilterapp.model.getReportProblemmodel.ResultItem;
import com.wm.camerafilterapp.network.ApiInterface;
import com.wm.camerafilterapp.ui.auth.LoginActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.utils.UrlApi;
import com.wm.camerafilterapp.viewmodel.ReportProblemViewModel;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ReportProblemActivity extends AppCompatActivity implements View.OnClickListener
{
     ActivityReportProblemBinding binding;
    private ArrayAdapter<String> timeSlotAdapter;
    ArrayList<String> servicesnamelist = new ArrayList<>();
    ArrayList<String> servicesidlist = new ArrayList<>();
    ProgressD progressDialog;
    private ReportProblemViewModel reportProblemViewModel;
    private List<ResultItem> reasonlist;
    String reason_id;
     @Override
     protected void onCreate(Bundle savedInstanceState)
     {
        super.onCreate(savedInstanceState);
         binding = DataBindingUtil.setContentView(this, R.layout.activity_report_problem);
         binding.backIcon.setOnClickListener(this);
         binding.autoCompleteState.setOnClickListener(this);
         binding.btnSubmit.setOnClickListener(this);
         reportProblemViewModel = new ViewModelProvider(this).get(ReportProblemViewModel.class);
         if(CommonMethod.isOnline(ReportProblemActivity.this))
         {
             getReportProblemApi();
         }
         else
         {
             CommonMethod.showAlert(getString(R.string.check_internet), ReportProblemActivity.this);
         }
     }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.back_icon:
                finish();
                break;

            case R.id.btn_submit:
               // Toast.makeText(this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                if(CommonMethod.isOnline(ReportProblemActivity.this))
                {
                    hideKeyboard((Button)v);
                    if (validation())
                    {
                       // ReviewApi(binding.reviewedittext.getText().toString().trim(),String.valueOf(binding.ratingbar.getRating()));
                        ReportProblemApi(LoginPreferences.getActiveInstance(this).getUserId(),
                                reason_id,binding.reviewedittext.getText().toString().trim());
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), ReportProblemActivity.this);
                }
                break;

            case R.id.autoCompleteState:
                initilizeStateSpinner();
                break;
        }
    }

    private boolean validation()
    {
        /*if(TextUtils.isEmpty(binding.reviewedittext.getText().toString().trim()))
        {
            Toast.makeText(this, "Please Enter Review", Toast.LENGTH_SHORT).show();
            return false;
        }*/
        return true;
    }


    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }

    private void ReportProblemApi(String userId,String selectedissueId, String comment)
    {
        progressDialog = ProgressD.show(ReportProblemActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        reportProblemViewModel.PostReport(userId,selectedissueId,comment).observe(this, PostReportProblemResponse ->
        {
            progressDialog.dismiss();
            if(!PostReportProblemResponse.isError())
            {
                Toast.makeText(ReportProblemActivity.this, PostReportProblemResponse.getMessage(), Toast.LENGTH_SHORT).show();
                finish();
            }
            else if(PostReportProblemResponse.getCode()==400)
            {
                Toast.makeText(ReportProblemActivity.this,getString(R.string.pleaseselectid), Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(ReportProblemActivity.this, PostReportProblemResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initilizeStateSpinner()
    {
        timeSlotAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, servicesnamelist);
        binding.autoCompleteState.setAdapter(timeSlotAdapter);
        binding.autoCompleteState.requestFocus();
        binding.autoCompleteState.showDropDown();
        binding.autoCompleteState.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                binding.autoCompleteState.setText(servicesnamelist.get(position));
                String service_name=servicesnamelist.get(position);
                reason_id=servicesidlist.get(position);
            }
        });
    }

    public void getReportProblemApi()
    {
        final ProgressD progressDialog = ProgressD.show(ReportProblemActivity.this, getResources().getString(R.string.logging_in), true, false, null);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlApi.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<GetIssueListResponse> call = service.GetIssueList();
        call.enqueue(new Callback<GetIssueListResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<GetIssueListResponse> call, Response<GetIssueListResponse> response) {
                progressDialog.dismiss();
                try {
                    GetIssueListResponse resultFile = response.body();
                    if (!resultFile.isError())
                    {
                        reasonlist = resultFile.getResult();
                        for(int i = 0; i < reasonlist.size(); i++)
                        {
                            servicesnamelist.add(reasonlist.get(i).getTitle());
                            servicesidlist.add(String.valueOf(reasonlist.get(i).getId()));
                        }
                    }
                } catch (Exception e) {
                    Log.e("Login Faild", e.toString());
                }
            }

            @Override
            public void onFailure(Call<GetIssueListResponse> call, Throwable t) {
                Toast.makeText(ReportProblemActivity.this, "Failed" + t, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

}