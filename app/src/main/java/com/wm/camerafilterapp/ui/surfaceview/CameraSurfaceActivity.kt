package com.wm.camerafilterapp.ui.surfaceview


import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.hardware.Camera
import android.hardware.Camera.CameraInfo
import android.os.Bundle
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.wm.camerafilterapp.R
import com.wm.camerafilterapp.ui.home.HomeActivity
import com.wm.camerafilterapp.utils.CommonMethod
import com.wm.camerafilterapp.utils.ProgressD
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException


class CameraSurfaceActivity : AppCompatActivity(), SurfaceHolder.Callback, Camera.PictureCallback
{
    private var surfaceHolder: SurfaceHolder? = null
    private var camera: Camera? = null
    private var isFlashOn = false
    var finalFile: File? = null
    private lateinit var surfaceView:SurfaceView
    private lateinit var ivClose1:ImageView
    private lateinit var ivClose2:ImageView
    private lateinit var ivSave:ImageView
    private lateinit var ivReloadPicture:ImageView
    private lateinit var ivFlash:ImageView
    private lateinit var ivPreview:ImageView
    private lateinit var ivTakePicture:ImageView
    private lateinit var clSurfaceLayout: ConstraintLayout
    private lateinit var ivSwitchcamera: ImageView
    private lateinit var image_view1: ImageView

    private var frontCamera = false

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_surface)
        surfaceView=findViewById(R.id.surfaceView)
        ivClose1=findViewById(R.id.ivClose1)
        ivClose2=findViewById(R.id.ivClose2)
        ivSave=findViewById(R.id.ivSave)
        ivReloadPicture=findViewById(R.id.ivReloadPicture)
        ivFlash=findViewById(R.id.ivFlash)
        ivPreview=findViewById(R.id.ivPreview)
        ivTakePicture=findViewById(R.id.ivTakePicture)
        clSurfaceLayout=findViewById(R.id.clSurfaceLayout)
        ivSwitchcamera=findViewById(R.id.ivSwitchcamera)
        image_view1=findViewById(R.id.image_view1)

        setupSurfaceHolder()

        ivClose1.setOnClickListener{
           // finish()
            finishAffinity()
        }




        ivClose2.setOnClickListener{
            image_view1.visibility=View.GONE
            surfaceView.visibility=View.VISIBLE
            ivClose1.visibility=View.VISIBLE
            ivClose2.visibility=View.GONE



            ivPreview.visibility = View.GONE
            ivReloadPicture.visibility = View.GONE
            ivSave.visibility = View.GONE
            ivTakePicture.visibility = View.VISIBLE

            clSurfaceLayout.visibility=View.VISIBLE
            resetCamera()
        }

        ivFlash.setOnClickListener {
            setFlashOnOff()
        }

        ivSave.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            //   intent.putExtra("image", finalFile?.absolutePath)
            startActivity(intent)
        }

        ivReloadPicture.setOnClickListener {

            ivClose1.visibility=View.VISIBLE
            ivClose2.visibility=View.GONE
            surfaceView.visibility=View.VISIBLE
            image_view1.visibility=View.GONE


            ivPreview.visibility = View.GONE
            ivReloadPicture.visibility = View.GONE
            ivSave.visibility = View.GONE
            ivTakePicture.visibility = View.VISIBLE

            clSurfaceLayout.visibility=View.VISIBLE
            resetCamera()
        }
        ivTakePicture.setOnClickListener { captureImage() }
    }

    private fun setupSurfaceHolder()
    {
        surfaceHolder = surfaceView!!.holder
        surfaceHolder!!.addCallback(this)

      //  setBtnClick()
    }
    private fun captureImage() {
        if (camera != null) {
            camera!!.autoFocus { success, camera ->
                if (success) {
                    camera!!.takePicture(null, null, this)
                }
            }
        }
    }

    override fun surfaceCreated(surfaceHolder: SurfaceHolder)
    {
        startCamera()
    }

    private fun setFlashOnOff() {
        if (isFlashOn) {
            val params: Camera.Parameters = camera!!.getParameters()
            params.flashMode = Camera.Parameters.FLASH_MODE_OFF
        } else {
            val params: Camera.Parameters = camera!!.getParameters()
            params.flashMode = Camera.Parameters.FLASH_MODE_TORCH
        }

    }

    private fun startCamera()
    {
        camera = Camera.open()
        camera!!.setDisplayOrientation(90)
        try {
            camera!!.setPreviewDisplay(surfaceHolder)
            camera!!.startPreview()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    override fun surfaceChanged(surfaceHolder: SurfaceHolder, i: Int, i1: Int, i2: Int) {
        resetCamera()
    }

    private fun resetCamera() {
        if (surfaceHolder!!.surface == null) {
            // Return if preview surface does not exist
            return
        }
        // Stop if preview surface is already running.
        camera!!.stopPreview()
        try {
            // Set preview display
            camera!!.setPreviewDisplay(surfaceHolder)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        // Start the camera preview...
        camera!!.startPreview()
    }

    override fun surfaceDestroyed(surfaceHolder: SurfaceHolder) {
        releaseCamera()
    }

    private fun releaseCamera() {
        camera!!.stopPreview()
        camera!!.release()
        camera = null
    }

    override fun onPictureTaken(bytes: ByteArray, camera: Camera) {
     //   bytesarray = bytes
        saveImage(bytes)
    }

    private fun saveImage(bytes: ByteArray)
    {
        var isPhotoOk = true
        val progressDialog = ProgressD.show(this, resources.getString(R.string.logging_in), true, false, null)
        Coroutines.runOnIO {
            try {
               /* val fileName = "WINE_" + System.currentTimeMillis() + ".jpg"
                finalFile = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName)
                outStream = FileOutputStream(finalFile)
                outStream.write(bytes)
                outStream.close()*/


                val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
                val matrix = Matrix()
                matrix.postRotate(90F)
                val resized = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
                CommonMethod.rotatedBitmap = Bitmap.createScaledBitmap(resized, 480, 720, true)

               /*val fileName = "WINE_" + System.currentTimeMillis() + ".jpg"
                finalFile = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName)

                outStream = FileOutputStream(finalFile)
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
                outStream.flush()
                outStream.close()*/
            }
            catch (e: FileNotFoundException) {
                e.printStackTrace()
                isPhotoOk = false
            }
            catch (e: Exception)
            {
                e.printStackTrace()
                isPhotoOk = false
            }

            Coroutines.runOnMain {
                if (isPhotoOk)
                {
                    surfaceView.visibility=View.GONE
                    image_view1.visibility=View.VISIBLE
                    image_view1.setImageBitmap(CommonMethod.rotatedBitmap)
                  //  image_view1.setScaleType(ImageView.ScaleType.FIT_XY)

                    progressDialog.dismiss()
                    ivReloadPicture.visibility = View.VISIBLE
                    ivClose2.visibility = View.VISIBLE
                    ivClose1.visibility=View.GONE

                    clSurfaceLayout.visibility=View.GONE
                    ivSave.visibility = View.VISIBLE
                    ivTakePicture.visibility = View.GONE
                }
            }
        }
    }
    override fun onBackPressed()
    {
        super.onBackPressed()
        finish()
        finishAffinity()
    }





    private fun getFrontCameraId(): Int {
        val availableCameras = Camera.getNumberOfCameras()
        val info = CameraInfo()
        for (i in 0 until availableCameras) {
            Camera.getCameraInfo(i, info)
            if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
                frontCamera = false
                return i
            }
        }
        return -1
    }

    private fun defaultBackCamera(): Int
    {
        val numberOfCameras = Camera.getNumberOfCameras()
        val cameraInfo = CameraInfo()
        for (i in 0 until numberOfCameras) {
            Camera.getCameraInfo(i, cameraInfo)
            if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK) {
                frontCamera = true
                return i
            }
        }
        return -1
    }
}