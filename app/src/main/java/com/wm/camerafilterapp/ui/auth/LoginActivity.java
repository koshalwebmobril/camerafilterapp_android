package com.wm.camerafilterapp.ui.auth;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;
import com.wm.camerafilterapp.ui.baseActivity.BaseActivity;
import com.wm.camerafilterapp.ui.surfaceview.CameraSurfaceActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.AccountViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.wm.camerafilterapp.utils.CommonMethod.isValidEmaillId;

public class LoginActivity extends BaseActivity
{
    @BindView(R.id.sign_in_text)
    TextView signInText;
    @BindView(R.id.txtcontinuetoguestuser)
    TextView txtcontinuetoguestuser;
    @BindView(R.id.edittext_email)
    EditText edittextEmail;
    @BindView(R.id.input_layout)
    LinearLayout inputLayout;
    @BindView(R.id.edittext_password)
    EditText edittextPassword;
    @BindView(R.id.checkbox_rembare)
    CheckBox checkboxRembare;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.img_password)
    ImageView img_password;
    

    private boolean isPasswordShowing = false;
    ProgressD progressDialog;
    private AccountViewModel accountViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
    }

    @OnClick({R.id.btn_login, R.id.forgot_password, R.id.txt_signup, R.id.txtcontinuetoguestuser
            ,R.id.img_password,R.id.checkbox_rembare})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_login:
                String email=edittextEmail.getText().toString().trim();
                String password=edittextPassword.getText().toString().trim();
                if(CommonMethod.isOnline(LoginActivity.this))
                {
                    hideKeyboard((Button)view);
                    Dexter.withContext(this)
                            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .withListener(new MultiplePermissionsListener() {
                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {
                                    if (report.areAllPermissionsGranted())
                                    {
                                        if (validation())
                                            {
                                                LoginApi(email,password);
                                            }
                                    }
                                    if (report.isAnyPermissionPermanentlyDenied())
                                    {
                                        showSettingsDialog();
                                    }
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                    permissionToken.continuePermissionRequest();
                                }
                            }).check();
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), LoginActivity.this);
                }
                break;

            case R.id.txt_signup:
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
                break;

            case R.id.forgot_password:
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                break;


            case R.id.txtcontinuetoguestuser:
                Intent intentguest = new Intent(LoginActivity.this, GuestUserActivity.class);
                startActivity(intentguest);
                break;


            case R.id.img_password:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    edittextPassword.setTransformationMethod(new PasswordTransformationMethod());
                    img_password.setImageResource(R.drawable.password_invisible1);
                    edittextPassword.setSelection(edittextPassword.getText().length());
                }
                else
                    {
                    isPasswordShowing = true;
                    edittextPassword.setTransformationMethod((TransformationMethod) null);
                    img_password.setImageResource(R.drawable.password_visible1);
                    edittextPassword.setSelection(edittextPassword.getText().length());
                  }
                break;


            case R.id.checkbox_rembare:
                 break;
        }
    }
    private boolean validation()
    {
        if(TextUtils.isEmpty(edittextEmail.getText().toString().trim()))
        {
            toastShort(getString(R.string.edittextstr));
            return false;
        }
        else if (!isValidEmaillId(edittextEmail.getText().toString().trim()))
        {
            toastShort(getString(R.string.validemailid));
            return false;
        }


        else if(TextUtils.isEmpty(edittextPassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enterpasswordlogin));
            return false;
        }
        else if (edittextPassword.getText().toString().trim().length() < 8 || edittextPassword.getText().toString().trim().length() > 16)
        {
            toastShort(getString(R.string.passwordvalidation));
            return false;
        }
        return true;
    }


    private void LoginApi(String email_address, String password)
    {
        progressDialog = ProgressD.show(LoginActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        accountViewModel.LoginUser(email_address,password,"2","wdd").observe(this, LoginResponse ->
        {
            progressDialog.dismiss();
            if(LoginResponse!=null&&LoginResponse.getCode()==200)
            {
                Toast.makeText(LoginActivity.this, LoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                Intent i=new Intent(LoginActivity.this, CameraSurfaceActivity.class);
                startActivity(i);
                finish();
                LoginPreferences.getActiveInstance(LoginActivity.this).setToken(LoginResponse.getResult().getToken());
                LoginPreferences.getActiveInstance(LoginActivity.this).setUserEmail(LoginResponse.getResult().getEmail());
                LoginPreferences.getActiveInstance(LoginActivity.this).setUserId(String.valueOf(LoginResponse.getResult().getId()));
            }
            else
            {
                Toast.makeText(LoginActivity.this, LoginResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        finish();
    }

    private void showSettingsDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) ->
        {
            dialog.cancel();
            openSettings();
        });

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}