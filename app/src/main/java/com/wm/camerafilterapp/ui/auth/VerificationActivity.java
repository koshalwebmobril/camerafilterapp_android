package com.wm.camerafilterapp.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.ui.baseActivity.BaseActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.AccountViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.aabhasjindal.otptextview.OtpTextView;

public class VerificationActivity extends BaseActivity {

    @BindView(R.id.forgotpassword)
    TextView forgotpassword;
    @BindView(R.id.otp_view)
    OtpTextView otpView;
    @BindView(R.id.btn_verification)
    Button btnVerification;
    @BindView(R.id.txt_signup)
    TextView txtSignup;

    @BindView(R.id.emailidhidetxt)
    TextView emailidhidetxt;

    @BindView(R.id.img_back)
    ImageView img_back;

    String otpuser;
    ProgressD progressDialog;
    private AccountViewModel accountViewModel;
    String email_id;
    String mask = "*****";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        ButterKnife.bind(this);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
        email_id=getIntent().getStringExtra("email");
        int at = email_id.indexOf("@");
        if (at > 2)
        {
            final int maskLen = Math.min(Math.max(at / 2, 2), 4);
            final int start = (at - maskLen) / 2;
            emailidhidetxt.setText(email_id.substring(0, start) + mask.substring(0, maskLen) + email_id.substring(start + maskLen));
        }
    }

    @OnClick({R.id.btn_verification,R.id.txt_signup,R.id.img_back})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.btn_verification:
                if (CommonMethod.isOnline(VerificationActivity.this))
                {
                    otpuser=otpView.getOTP();
                    hideKeyboard((Button)view);
                    if(otpuser.length() < 4)
                    {
                        Toast.makeText(VerificationActivity.this, getString(R.string.otpusertxt), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else
                    {
                        OtpVerifyApi();
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), VerificationActivity.this);
                }
                break;

            case R.id.txt_signup:
                ForgotPasswordApi(email_id);
                break;

            case R.id.img_back:
                finish();
                break;

        }
    }

    private void ForgotPasswordApi(String email_address)
    {
        progressDialog = ProgressD.show(VerificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        accountViewModel.ForgotPassword(email_address).observe(this, ForgotpasswordResponse ->
        {
            progressDialog.dismiss();
            toastShort(ForgotpasswordResponse.getMessage());
            if(ForgotpasswordResponse !=null&&ForgotpasswordResponse.getCode()==200)
            {

            }
            else
            { }
        });
    }



    private void OtpVerifyApi()
    {
        progressDialog = ProgressD.show(VerificationActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        accountViewModel.VerificationOtp(getIntent().getStringExtra("email"),otpuser).observe(this, VerificationResponse ->
        {
            progressDialog.dismiss();
            toastShort(VerificationResponse.getMessage());
            if(VerificationResponse !=null&&VerificationResponse.getCode()==200)
            {
                Intent i=new Intent(VerificationActivity.this, ResetPasswordActivity.class);
                i.putExtra("email",email_id);
                startActivity(i);
                finish();
            }
            else
            { }
        });
    }
}