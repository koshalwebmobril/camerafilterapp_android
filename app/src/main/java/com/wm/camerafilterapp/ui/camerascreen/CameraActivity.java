package com.wm.camerafilterapp.ui.camerascreen;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.ui.home.HomeActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CameraActivity extends AppCompatActivity
{
    static final int REQUEST_CAPTURE_IMAGE = 1;
    private String imageFilePath;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        launchCameraIntent();
    }
    private void launchCameraIntent()
    {
        Intent pictureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (pictureIntent.resolveActivity(this.getPackageManager()) != null) {
            //Create a file to store the image
            File file = null;
            try
            {
                file = createImageFile();

            } catch (IOException ex)
            { }
            if(file != null)
            {
                Uri photoURI = FileProvider.getUriForFile(CameraActivity.this, getPackageName() + ".provider", file);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(pictureIntent, REQUEST_CAPTURE_IMAGE);
            }
        }
    }
    private File createImageFile() throws IOException
    {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        imageFilePath = image.getAbsolutePath();
        return image;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == REQUEST_CAPTURE_IMAGE)
            {
               File file = new File(imageFilePath);
               Intent i=new Intent(CameraActivity.this, HomeActivity.class);
               i.putExtra("image",imageFilePath);
               startActivity(i);
               finish();
            }
        }
    }
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finishAffinity();
        finish();
    }
}