package com.wm.camerafilterapp.ui.auth;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wm.camerafilterapp.R;


import com.wm.camerafilterapp.databinding.ActivityResetPasswordBinding;
import com.wm.camerafilterapp.ui.baseActivity.BaseActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.AccountViewModel;

public class ResetPasswordActivity extends BaseActivity implements View.OnClickListener
{
    private ActivityResetPasswordBinding binding;
    private AccountViewModel accountViewModel;
    ProgressD progressDialog;
    private boolean isPasswordShowing = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
        binding.btnSubmit.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);
        binding.imgPassword.setOnClickListener(this);
        binding.imgConfirmpassword.setOnClickListener(this);
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                String newpassword=binding.edittextPassword.getText().toString().trim();
                String confirmpassword=binding.confirmpassword.getText().toString().trim();
                if(CommonMethod.isOnline(ResetPasswordActivity.this))
                {
                     hideKeyboard((Button)v);
                    if (validation())
                    {
                        ResetPasswordApi(getIntent().getStringExtra("email"),newpassword,confirmpassword);
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), ResetPasswordActivity.this);
                }
                break;


            case R.id.img_oldpassword:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    binding.edittextOldpassword.setTransformationMethod(new PasswordTransformationMethod());
                    binding.imgOldpassword.setImageResource(R.drawable.password_invisible1);
                    binding.edittextOldpassword.setSelection(binding.edittextOldpassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    binding.edittextOldpassword.setTransformationMethod((TransformationMethod) null);
                    binding.imgOldpassword.setImageResource(R.drawable.password_visible1);
                    binding.edittextOldpassword.setSelection(binding.edittextOldpassword.getText().length());
                }
                break;


            case R.id.img_password:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    binding.edittextPassword.setTransformationMethod(new PasswordTransformationMethod());
                    binding.imgPassword.setImageResource(R.drawable.password_invisible1);
                    binding.edittextPassword.setSelection(binding.edittextPassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    binding.edittextPassword.setTransformationMethod((TransformationMethod) null);
                    binding.imgPassword.setImageResource(R.drawable.password_visible1);
                    binding.edittextPassword.setSelection(binding.edittextPassword.getText().length());
                }
                break;



            case R.id.img_confirmpassword:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    binding.confirmpassword.setTransformationMethod(new PasswordTransformationMethod());
                    binding.imgConfirmpassword.setImageResource(R.drawable.password_invisible1);
                    binding.confirmpassword.setSelection(binding.confirmpassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    binding.confirmpassword.setTransformationMethod((TransformationMethod) null);
                    binding.imgConfirmpassword.setImageResource(R.drawable.password_visible1);
                    binding.confirmpassword.setSelection(binding.confirmpassword.getText().length());
                }
                break;


            case R.id.img_back:
               finish();
        }
    }



    private boolean validation()
    {
        if(TextUtils.isEmpty(binding.edittextPassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enterpassword));
            return false;
        }
        else if (binding.edittextPassword.getText().toString().trim().length() < 8 || binding.edittextPassword.getText().toString().trim().length() > 16)
        {
            toastShort(getString(R.string.passwordvalidation));
            return false;
        }

        else if(TextUtils.isEmpty(binding.confirmpassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enterconfirmpassword));
            return false;
        }
        else if (binding.confirmpassword.getText().toString().trim().length() < 8 || binding.confirmpassword.getText().toString().trim().length() > 16)
        {
            toastShort(getString(R.string.confirmpasswordvalidation));
            return false;
        }
        else if (!binding.edittextPassword.getText().toString().trim().equals(binding.confirmpassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.passwordconfirmmatch));
            return false;
        }
        return true;
    }

    private void ResetPasswordApi(String email, String newpassword, String confirmpassword)
    {
        progressDialog = ProgressD.show(ResetPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        accountViewModel.resetpassword(email,newpassword,confirmpassword).observe(this, ChangePasswordResponse ->
        {
            progressDialog.dismiss();
            if(ChangePasswordResponse !=null&&ChangePasswordResponse.getCode()==200)
            {
                Toast.makeText(ResetPasswordActivity.this, ChangePasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
                Intent i=new Intent(ResetPasswordActivity.this, LoginActivity.class);
                startActivity(i);
            }
            else
            {
                Toast.makeText(ResetPasswordActivity.this, ChangePasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }
}