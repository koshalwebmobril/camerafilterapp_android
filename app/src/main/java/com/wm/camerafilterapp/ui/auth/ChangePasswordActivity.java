package com.wm.camerafilterapp.ui.auth;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;
import com.wm.camerafilterapp.databinding.ActivityChangePasswordBinding;
import com.wm.camerafilterapp.databinding.ActivityProfileBinding;
import com.wm.camerafilterapp.ui.baseActivity.BaseActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.AccountViewModel;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    private ActivityChangePasswordBinding binding;
    ProgressD progressDialog;
    private AccountViewModel accountViewModel;
    private boolean isPasswordShowing = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        binding.btnSubmit.setOnClickListener(this);
        binding.imgOldpassword.setOnClickListener(this);
        binding.imgPassword.setOnClickListener(this);
        binding.imgConfirmpassword.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_submit:
                if(CommonMethod.isOnline(ChangePasswordActivity.this))
                {
                    String oldpassword=binding.edittextOldpassword.getText().toString().trim();
                    String newpassword=binding.edittextPassword.getText().toString().trim();
                    String confirmpassword=binding.confirmpassword.getText().toString().trim();
                    hideKeyboard((Button)v);
                    if (validation())
                    {
                        ChangePasswordApi(LoginPreferences.getActiveInstance(this).getToken(),
                                LoginPreferences.getActiveInstance(this).getUserEmail(),oldpassword,newpassword,confirmpassword);
                    }
                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), ChangePasswordActivity.this);
                }
                break;

            case R.id.img_oldpassword:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    binding.edittextOldpassword.setTransformationMethod(new PasswordTransformationMethod());
                    binding.imgOldpassword.setImageResource(R.drawable.password_invisible1);
                    binding.edittextOldpassword.setSelection(binding.edittextOldpassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    binding.edittextOldpassword.setTransformationMethod((TransformationMethod) null);
                    binding.imgOldpassword.setImageResource(R.drawable.password_visible1);
                    binding.edittextOldpassword.setSelection(binding.edittextOldpassword.getText().length());
                }
                break;


            case R.id.img_password:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    binding.edittextPassword.setTransformationMethod(new PasswordTransformationMethod());
                    binding.imgPassword.setImageResource(R.drawable.password_invisible1);
                    binding.edittextPassword.setSelection(binding.edittextPassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    binding.edittextPassword.setTransformationMethod((TransformationMethod) null);
                    binding.imgPassword.setImageResource(R.drawable.password_visible1);
                    binding.edittextPassword.setSelection(binding.edittextPassword.getText().length());
                }
                break;


            case R.id.img_confirmpassword:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    binding.confirmpassword.setTransformationMethod(new PasswordTransformationMethod());
                    binding.imgConfirmpassword.setImageResource(R.drawable.password_invisible1);
                    binding.confirmpassword.setSelection(binding.confirmpassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    binding.confirmpassword.setTransformationMethod((TransformationMethod) null);
                    binding.imgConfirmpassword.setImageResource(R.drawable.password_visible1);
                    binding.confirmpassword.setSelection(binding.confirmpassword.getText().length());
                }
                break;

            case R.id.img_back:
                finish();
                break;
        }
    }

    public void hideKeyboard(View view)
    {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }


    private boolean validation()
    {

        if(TextUtils.isEmpty(binding.edittextOldpassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enteryouroldpassword));
            return false;
        }


        else if(TextUtils.isEmpty(binding.edittextPassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enterpassword));
            return false;
        }
        else if (binding.edittextPassword.getText().toString().trim().length() < 8 || binding.edittextPassword.getText().toString().trim().length() > 16)
        {
            toastShort(getString(R.string.passwordvalidation));
            return false;
        }

        else if(TextUtils.isEmpty(binding.confirmpassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enterconfirmpassword));
            return false;
        }
        else if (binding.confirmpassword.getText().toString().trim().length() < 8 || binding.confirmpassword.getText().toString().trim().length() > 16)
        {
            toastShort(getString(R.string.confirmpasswordvalidation));
            return false;
        }
        else if (!binding.edittextPassword.getText().toString().trim().equals(binding.confirmpassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.passwordconfirmmatch));
            return false;
        }
        return true;
    }


    private void ChangePasswordApi(String token,String email_address,String old_password,String newpassword, String confirmpassword)
    {
        progressDialog = ProgressD.show(ChangePasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);

        accountViewModel.ChangePassword(token,email_address,old_password,newpassword,confirmpassword).observe(this, ChangePasswordResponse ->
        {
            progressDialog.dismiss();
            if(ChangePasswordResponse!=null && ChangePasswordResponse.getCode()==200)
            {
                Toast.makeText(ChangePasswordActivity.this, ChangePasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
                Intent i=new Intent(ChangePasswordActivity.this, LoginActivity.class);
                startActivity(i);
                LoginPreferences.deleteAllPreference();
            }
            else
            {
                Toast.makeText(ChangePasswordActivity.this, ChangePasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}