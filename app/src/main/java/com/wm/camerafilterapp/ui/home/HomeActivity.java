package com.wm.camerafilterapp.ui.home;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;


import com.github.chrisbanes.photoview.PhotoView;
import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.adapter.HomeAdapter;


import com.wm.camerafilterapp.databinding.ActivityHomeBinding;
import com.wm.camerafilterapp.interfase.CameraFilterListner;
import com.wm.camerafilterapp.model.filterimagenamemodel.FilterImageModel;
import com.wm.camerafilterapp.ui.auth.LoginActivity;
import com.wm.camerafilterapp.ui.profile.ProfileActivity;
import com.wm.camerafilterapp.ui.camerascreen.CameraActivity;
import com.wm.camerafilterapp.ui.surfaceview.CameraSurfaceActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;

import net.alhazmy13.imagefilter.ImageFilter;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.IntBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityHomeBinding binding;
    HomeAdapter homeAdapter;
    ArrayList<FilterImageModel> filterimagelist;
    private static final int PERMISSION_REQ_CODE = 1<<2;
    private String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,};
    int position1;
    Animation zoomAnimation;
    int zoomselected=0;
    Bitmap zoombitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);

        binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);
        binding.imageView1.setScaleType(ImageView.ScaleType.FIT_XY);

        binding.profileIcon.setOnClickListener(this);
        binding.backIcon.setOnClickListener(this);
        binding.cancelButton.setOnClickListener(this);
        binding.applyButton.setOnClickListener(this);
        binding.save.setOnClickListener(this);
        binding.refereshIcon.setOnClickListener(this);
        binding.timerIcon.setOnClickListener(this);

        filterimagelist=new ArrayList<FilterImageModel>();
        getFilterImage();
        binding.recyclerviewFilter.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        homeAdapter = new HomeAdapter(this, filterimagelist, new CameraFilterListner()
        {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onSelectFilter(int id,int position)
            {
                homeAdapter.notifyDataSetChanged();
                position1=position;
                binding.relativeCancelApplyUnhide.setVisibility(View.VISIBLE);
                if(id==1)
                {
                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);

                    binding.imageView1.setColorFilter(null);
                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);
                }
                else if(id==2)
                {
                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);

                    ColorMatrix matrix = new ColorMatrix();
                    matrix.setSaturation(0);
                    ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);

                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);
                    binding.imageView1.setColorFilter(filter);

                 //   zoomselected=0;
                }
                else if(id==3)
                {
                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);

                    binding.imageView1.setColorFilter(null);
                    Bitmap bInput/*your input bitmap*/, bOutput;
                    Matrix matrix = new Matrix();
                    matrix.preScale(1.0f, -1.0f);
                    bOutput = Bitmap.createBitmap(CommonMethod.rotatedBitmap, 0, 0, CommonMethod.rotatedBitmap.getWidth(), CommonMethod.rotatedBitmap.getHeight(), matrix, true);
                    binding.imageView1.setImageBitmap(bOutput);

                 //   zoomselected=0;
                }
                else if(id==4)     //skatch
                {
                    binding.imageView1.setColorFilter(null);
                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);

                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);
                    binding.imageView1.setImageBitmap(ImageFilter.applyFilter(CommonMethod.rotatedBitmap, ImageFilter.Filter.SKETCH));
                    //    zoomselected=0;
                }
                else if(id==5)       //dim filter
                {
                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);

                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);
                    binding.imageView1.setColorFilter(getColor(R.color.light_grey), PorterDuff.Mode.LIGHTEN);

               //     zoomselected=0;

                }

                else if(id==6)                         //fish eye lens
                {
                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);

                    binding.imageView1.setColorFilter(null);
                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);
                    Toast.makeText(HomeActivity.this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();

                  //  zoomselected=0;
                }

                else if(id==7)            //invert color
                {
                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);

                    binding.imageView1.setColorFilter(null);
                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);
                    binding.imageView1.setColorFilter(new ColorMatrixColorFilter(NEGATIVE));

                   // zoomselected=0;
                }
                else if(id==8)                      // 5x filter
                {
                    binding.imageView1.setColorFilter(null);
                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);

                     zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom);
                     binding.imageView1.startAnimation(zoomAnimation);

                   /*  BitmapDrawable draw = (BitmapDrawable)  binding.imageView1.getDrawable();
                     zoombitmap = draw.getBitmap();*/

                  //  zoomselected=1;
                }

                else if(id==9)                   // Pixellated Filter
                {
                    binding.imageView1.setColorFilter(null);
                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);


                    zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                    binding.imageView1.startAnimation(zoomAnimation);
                    Toast.makeText(HomeActivity.this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();

                 //   zoomselected=0;
                }

                else if(id==10)                   // Glass over effect
                {
                    binding.imageView1.setColorFilter(null);
                    binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);
                    Toast.makeText(HomeActivity.this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();

                //    zoomselected=0;
                }
            }
        });
        binding.recyclerviewFilter.setAdapter(homeAdapter);
    }

    private static final float[] NEGATIVE = {
            -1.0f,     0,     0,    0, 255, // red
            0, -1.0f,     0,    0, 255, // green
            0,     0, -1.0f,    0, 255, // blue
            0,     0,     0, 1.0f,   0  // alpha
    };



    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.profile_icon:
                Intent i = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(i);
                break;

            case R.id.back_icon:
                Intent backintent=new Intent(HomeActivity.this, CameraSurfaceActivity.class);
                startActivity(backintent);
                finish();
                break;

            case R.id.cancel_button:
                homeAdapter.notifyDataSetChanged();
                filterimagelist.get(position1).setStatus(0);
                binding.relativeCancelApplyUnhide.setVisibility(View.INVISIBLE);
                binding.relativeCancelApplyHide.setVisibility(View.VISIBLE);

                zoomAnimation = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.zoom_out);
                binding.imageView1.startAnimation(zoomAnimation);

                binding.imageView1.setColorFilter(null);
                binding.imageView1.setImageBitmap(CommonMethod.rotatedBitmap);

             //   zoomselected=0;
                break;

            case R.id.apply_button:
                binding.linearTransparent.setVisibility(View.GONE);
                binding.linearRecyclerview.setVisibility(View.GONE);
                binding.relativeCancelApplyHide.setVisibility(View.VISIBLE);
                binding.relativeCancelApplyUnhide.setVisibility(View.GONE);
                binding.linearIcons.setVisibility(View.VISIBLE);
                break;


            case R.id.referesh_icon:
                Intent cameraintent1 = new Intent(HomeActivity.this, CameraSurfaceActivity.class);
                startActivity(cameraintent1);
                finish();
                break;


            case R.id.save:
                checkPermission();
                break;

            case R.id.timer_icon:
                Toast.makeText(this,getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                break;
        }
    }
    private void refreshGallary(File file)
    {
        Intent i=new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        i.setData(Uri.fromFile(file)); sendBroadcast(i);
    }

    private File getdisc()
    {
        File file= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        return new File(file,"Magnifeye");
    }

    private Bitmap viewToBitmap(View view, int widh, int hight)
    {
        Bitmap bitmap=Bitmap.createBitmap(widh,hight, Bitmap.Config.ARGB_8888);
        Canvas canvas=new Canvas(bitmap); view.draw(canvas);
        return bitmap;
    }

    public void getFilterImage()
    {
        FilterImageModel filterimage1 = new FilterImageModel(R.drawable.no_filter_icon,getString(R.string.nofilter),1,0);
        filterimagelist.add(filterimage1);

        FilterImageModel filterimage2 = new FilterImageModel(R.drawable.blackandwhite,getString(R.string.blackandwhite),2,0);
        filterimagelist.add(filterimage2);

        FilterImageModel filterimage3 = new FilterImageModel(R.drawable.flipimage,getString(R.string.flipimage),3,0);
        filterimagelist.add(filterimage3);

        FilterImageModel filterimage4 = new FilterImageModel(R.drawable.turnphotointosketch,getString(R.string.turnsketch),4,0);
        filterimagelist.add(filterimage4);

        FilterImageModel filterimage5 = new FilterImageModel(R.drawable.dimfilter,getString(R.string.dimfilter),5,0);
        filterimagelist.add(filterimage5);

        FilterImageModel filterimage6 = new FilterImageModel(R.drawable.fish_eye,getString(R.string.fish_eye),6,0);
        filterimagelist.add(filterimage6);

        FilterImageModel filterimage7 = new FilterImageModel(R.drawable.invertcolor,getString(R.string.invertcolor),7,0);
        filterimagelist.add(filterimage7);

        FilterImageModel filterimage8 = new FilterImageModel(R.drawable.mandetory5x,getString(R.string.mandetory5x),8,0);
        filterimagelist.add(filterimage8);

        FilterImageModel filterimage9 = new FilterImageModel(R.drawable.pixellated,getString(R.string.pixellatedfilter),9,0);
        filterimagelist.add(filterimage9);

        FilterImageModel filterimage10 = new FilterImageModel(R.drawable.glass_over_mask,getString(R.string.glassovereffect),10,0);
        filterimagelist.add(filterimage10);
    }


    private void checkPermission()
    {
        boolean granted = true;
        for (String per : PERMISSIONS)
        {
            if (!permissionGranted(per))
            {
                granted = false;
                break;
            }
        }
        if(granted)
        {
            FileOutputStream fileOutputStream = null;
            File file = getdisc();
            if (!file.exists() && !file.mkdirs())
            {
                Toast.makeText(getApplicationContext(), "sorry can not make dir", Toast.LENGTH_LONG).show();
                return;
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyymmsshhmmss");
            String date = simpleDateFormat.format(new Date());
            String name = "img" + date + ".jpeg";
            String file_name = file.getAbsolutePath() + "/" + name;
            File new_file = new File(file_name);
            try
            {
                fileOutputStream = new FileOutputStream(new_file);
                Bitmap bitmap = viewToBitmap(binding.imageView1, binding.imageView1.getWidth(),  binding.imageView1.getHeight());
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                Toast.makeText(getApplicationContext(), getString(R.string.savedsuccessfully), Toast.LENGTH_LONG).show();
                fileOutputStream.flush();
                fileOutputStream.close();


               /* if(zoomselected==1)
                {
                    //    Bitmap bitmap = zoombitmap;
                    zoombitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                    Toast.makeText(getApplicationContext(), getString(R.string.savedsuccessfully), Toast.LENGTH_LONG).show();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }
                else
                {
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
                    Toast.makeText(getApplicationContext(), getString(R.string.savedsuccessfully), Toast.LENGTH_LONG).show();
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }*/
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            refreshGallary(file);
        }
        else
        {
            requestPermissions();
        }
    }

    private boolean permissionGranted(String permission)
    {
        return ContextCompat.checkSelfPermission(
                this, permission) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQ_CODE);
    }
    private void toastNeedPermissions() {
        Toast.makeText(this, "You Need Accept This Storage Permission", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQ_CODE)
        {
            boolean granted = true;
            for (int result : grantResults)
            {
                granted = (result == PackageManager.PERMISSION_GRANTED);
                if (!granted) break;
            }
            if (granted)
            { }
            else
            {
                toastNeedPermissions();
            }
        }
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent i=new Intent(HomeActivity.this, CameraSurfaceActivity.class);
        startActivity(i);
        finish();
    }
}