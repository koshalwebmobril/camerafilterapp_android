package com.wm.camerafilterapp.ui.auth;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.method.TransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;
import com.wm.camerafilterapp.ui.baseActivity.BaseActivity;
import com.wm.camerafilterapp.ui.surfaceview.CameraSurfaceActivity;
import com.wm.camerafilterapp.ui.termandcondition.TermAndConditionActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.AccountViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.wm.camerafilterapp.utils.CommonMethod.isValidEmaillId;

public class RegisterActivity extends BaseActivity
{
    @BindView(R.id.sign_up)
    TextView signUp;
    @BindView(R.id.edittext_email)
    EditText edittextEmail;
    @BindView(R.id.input_layout)
    LinearLayout inputLayout;

    @BindView(R.id.edittext_mobileno)
    EditText edittextMobileno;


    @BindView(R.id.edittext_password)
    EditText edittextPassword;
    @BindView(R.id.confirm_password)
    EditText confirmPassword;
    @BindView(R.id.checkbox_rembare)
    CheckBox checkboxRembare;
    @BindView(R.id.agreewith)
    TextView agreewith;
    @BindView(R.id.termandcondition)
    TextView termandcondition;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.txt_signin)
    TextView txtSignin;

    @BindView(R.id.img_password)
    ImageView img_password;

    @BindView(R.id.img_confirmpassword)
    ImageView img_confirmpassword;



    private AccountViewModel accountViewModel;
    ProgressD progressDialog;
    private boolean isPasswordShowing = false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);

    }
    @OnClick({R.id.txtcontinue_guest, R.id.btn_register, R.id.txt_signin,R.id.img_password,R.id.img_confirmpassword,R.id.termandcondition})
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.txtcontinue_guest:
                Intent intentguest=new Intent(RegisterActivity.this,GuestUserActivity.class);
                startActivity(intentguest);
                break;

            case R.id.btn_register:
                String email=edittextEmail.getText().toString().trim();
                String mobilenumber=edittextMobileno.getText().toString().trim();
                String password=edittextPassword.getText().toString().trim();

                if(CommonMethod.isOnline(RegisterActivity.this))
                {
                    hideKeyboard((Button)view);
                    Dexter.withContext(this)
                            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .withListener(new MultiplePermissionsListener() {
                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {
                                    if (report.areAllPermissionsGranted())
                                    {
                                        if (validation())
                                        {
                                            goSignUp(email,mobilenumber,password);
                                        }
                                    }
                                    if (report.isAnyPermissionPermanentlyDenied())
                                    {
                                        showSettingsDialog();
                                    }
                                }
                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                    permissionToken.continuePermissionRequest();
                                }
                            }).check();

                }
                else
                {
                    CommonMethod.showAlert(getString(R.string.check_internet), RegisterActivity.this);
                }
                   break;


            case R.id.txt_signin:
                Intent i=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(i);
                break;

            case R.id.img_password:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    edittextPassword.setTransformationMethod(new PasswordTransformationMethod());
                    img_password.setImageResource(R.drawable.password_invisible1);
                    edittextPassword.setSelection(edittextPassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    edittextPassword.setTransformationMethod((TransformationMethod) null);
                    img_password.setImageResource(R.drawable.password_visible1);
                    edittextPassword.setSelection(edittextPassword.getText().length());
                }
                break;

            case R.id.img_confirmpassword:
                if(isPasswordShowing)
                {
                    isPasswordShowing = false;
                    confirmPassword.setTransformationMethod(new PasswordTransformationMethod());
                    img_confirmpassword.setImageResource(R.drawable.password_invisible1);
                    confirmPassword.setSelection(confirmPassword.getText().length());
                }
                else
                {
                    isPasswordShowing = true;
                    confirmPassword.setTransformationMethod((TransformationMethod) null);
                    img_confirmpassword.setImageResource(R.drawable.password_visible1);
                    confirmPassword.setSelection(confirmPassword.getText().length());
                }
                break;


            case R.id.termandcondition:
                Intent termscondition=new Intent(RegisterActivity.this, TermAndConditionActivity.class);
                termscondition.putExtra("page_status","2");
                startActivity(termscondition);
                break;
        }
    }

    private void goSignUp(String email_address, String mobile_number, String password)
    {
        progressDialog = ProgressD.show(RegisterActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        accountViewModel.RegisterUser(email_address,mobile_number,password,"2","wdd").observe(this, registerResponse ->
        {
            progressDialog.dismiss();
            if(registerResponse!=null&&registerResponse.getCode()==200)
            {
                Intent i=new Intent(RegisterActivity.this, CameraSurfaceActivity.class);
                startActivity(i);
                finish();
                Toast.makeText(RegisterActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                LoginPreferences.getActiveInstance(RegisterActivity.this).setToken(registerResponse.getResult().getToken());
                LoginPreferences.getActiveInstance(RegisterActivity.this).setUserEmail(registerResponse.getResult().getEmail());
                LoginPreferences.getActiveInstance(RegisterActivity.this).setUserId(String.valueOf(registerResponse.getResult().getId()));
            }
            else
            {
                Toast.makeText(RegisterActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean validation()
    {
        if (TextUtils.isEmpty(edittextEmail.getText().toString().trim()))
        {
            toastShort(getString(R.string.edittextstr));
            return false;
        }
        else if (!isValidEmaillId(edittextEmail.getText().toString().trim()))
        {
            toastShort(getString(R.string.validemailid));
            return false;
        }
        else if(TextUtils.isEmpty(edittextMobileno.getText().toString().trim()))
        {
            toastShort(getString(R.string.mobilenostr));
            return false;
        }

        else if(edittextMobileno.getText().toString().trim().length()<6 || edittextMobileno.getText().toString().trim().length()>15 )
        {
            toastShort(getString(R.string.mobilenolength));
            return false;
        }
        else if(TextUtils.isEmpty(edittextPassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enterpassword));
            return false;
        }
        else if(edittextPassword.getText().toString().trim().length() < 8 || edittextPassword.getText().toString().trim().length() > 16)
        {
            toastShort(getString(R.string.passwordvalidation));
            return false;
        }
        else if (TextUtils.isEmpty(confirmPassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.enterconfirmpassword));
            return false;
        }
        else if(!confirmPassword.getText().toString().trim().equals(edittextPassword.getText().toString().trim()))
        {
            toastShort(getString(R.string.passwordconfirmmatch));
            return false;
        }
        else if(!checkboxRembare.isChecked())
        {
            toastShort(getString(R.string.termandconditiontext));
            return false;
        }
        return true;
    }


    private void showSettingsDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) ->
        {
            dialog.cancel();
            openSettings();
        });

        builder.setTitle(getString(R.string.dialog_permission_title));
        builder.setMessage(getString(R.string.dialog_permission_message));
        builder.setPositiveButton(getString(R.string.go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}