package com.wm.camerafilterapp.ui.auth;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProvider;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.ui.baseActivity.BaseActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.AccountViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.wm.camerafilterapp.utils.CommonMethod.isValidEmaillId;

public class ForgotPasswordActivity extends BaseActivity {

    @BindView(R.id.forgotpassword)
    TextView forgotpassword;
    @BindView(R.id.edittext_emailid)
    EditText edittext_emailid;
    @BindView(R.id.btn_send)
    Button btnSend;
    @BindView(R.id.img_back)
    ImageView img_back;

    ProgressD progressDialog;
    private AccountViewModel accountViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        accountViewModel = new ViewModelProvider(this).get(AccountViewModel.class);
        img_back.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               finish();
            }
        });
    }
    @OnClick(R.id.btn_send)
    public void onClick(View view)
    {
        String email=edittext_emailid.getText().toString().trim();
        if(CommonMethod.isOnline(ForgotPasswordActivity.this))
        {
            hideKeyboard((Button)view);
            if (validation())
            {
                ForgotPasswordApi(email);
            }
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), ForgotPasswordActivity.this);
        }
    }

    private boolean validation()
    {
        if(TextUtils.isEmpty(edittext_emailid.getText().toString().trim()))
        {
            toastShort(getString(R.string.edittextstr));
            return false;
        }
        else if(!isValidEmaillId(edittext_emailid.getText().toString().trim()))
        {
            toastShort(getString(R.string.validemailid));
            return false;
        }
        return true;
    }

    private void ForgotPasswordApi(String email_address)
    {
        progressDialog = ProgressD.show(ForgotPasswordActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        accountViewModel.ForgotPassword(email_address).observe(this, ForgotpasswordResponse ->
        {
            progressDialog.dismiss();
            toastShort(ForgotpasswordResponse.getMessage());
            if(ForgotpasswordResponse !=null&&ForgotpasswordResponse.getCode()==200)
            {
                 Intent i=new Intent(ForgotPasswordActivity.this,VerificationActivity.class);
                 i.putExtra("email",email_address);
                startActivity(i);
                edittext_emailid.setText("");
            }
            else
            { }
        });
    }
}