package com.wm.camerafilterapp.ui.profile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;
import com.wm.camerafilterapp.databinding.ActivityProfileBinding;
import com.wm.camerafilterapp.ui.auth.ChangePasswordActivity;
import com.wm.camerafilterapp.ui.auth.LoginActivity;
import com.wm.camerafilterapp.ui.feedback.FeedbackActivity;
import com.wm.camerafilterapp.ui.howtoplay.HowtoplayActivity;
import com.wm.camerafilterapp.ui.reportproblem.ReportProblemActivity;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener
{
    private ActivityProfileBinding binding;
    Dialog dialog_alert;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        binding.btnFinished.setOnClickListener(this);
        binding.crossIcon.setOnClickListener(this);
        binding.linearLeaveusreview.setOnClickListener(this);
        binding.linearHowtoplay.setOnClickListener(this);
        binding.linearChangepassword.setOnClickListener(this);
        binding.linearPlaygame.setOnClickListener(this);
        binding.linearShare.setOnClickListener(this);
        binding.linearReportaproblem.setOnClickListener(this);
        binding.twitterShare.setOnClickListener(this);
        binding.facebookShare.setOnClickListener(this);
        binding.instagrameShare.setOnClickListener(this);
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_finished:
                openAlertDailog();
                break;


            case R.id.linear_howtoplay:
                Intent howtoplay=new Intent(ProfileActivity.this, HowtoplayActivity.class);
                startActivity(howtoplay);
                break;


            case R.id.cross_icon:
                 finish();
                 break;


            case R.id.linear_leaveusreview:
                Intent leaveusreview=new Intent(ProfileActivity.this, FeedbackActivity.class);
                startActivity(leaveusreview);
                break;


            case R.id.linear_changepassword:
                Intent changepassword=new Intent(ProfileActivity.this, ChangePasswordActivity.class);
                startActivity(changepassword);
                break;


            case R.id.linear_playgame:
                Toast.makeText(this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                break;

            case R.id.linear_share:
                Toast.makeText(this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                break;

            case R.id.linear_reportaproblem:
                Intent reportproblem=new Intent(ProfileActivity.this, ReportProblemActivity.class);
                startActivity(reportproblem);
                break;

            case R.id.twitter_share:
                Toast.makeText(this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                break;

            case R.id.facebook_share:
                Toast.makeText(this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                break;

            case R.id.instagrame_share:
                Toast.makeText(this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void openAlertDailog()
    {
        dialog_alert = new Dialog(ProfileActivity.this);
        dialog_alert.requestWindowFeature(1);
        dialog_alert.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog_alert.setContentView(R.layout.item_alert_dailog_finished);
        Button yes_btn = (Button) dialog_alert.findViewById(R.id.yes);
        Button no_btn = (Button) dialog_alert.findViewById(R.id.no);

        no_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_alert.dismiss();
            }
        });

        yes_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                 dialog_alert.dismiss();
                 Intent i=new Intent(ProfileActivity.this,LoginActivity.class);
                 startActivity(i);
                 LoginPreferences.deleteAllPreference();
            }
        });
        dialog_alert.show();
        dialog_alert.setCanceledOnTouchOutside(false);
    }
}