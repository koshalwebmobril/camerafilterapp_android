package com.wm.camerafilterapp.ui.surfaceview

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
object Coroutines
{
    fun runOnMain(work: suspend (() -> Unit)) =
             CoroutineScope(Dispatchers.Main).launch {
                work()
            }
    fun runOnIO(work: suspend (() -> Unit)) =
            CoroutineScope(Dispatchers.IO).launch {
                work()
            }

    fun runOnAsyncIO(work: suspend (() -> Unit)) =
            CoroutineScope(Dispatchers.IO).async {
                work()
            }

    fun runOnDefault(work: suspend (() -> Unit)) =
            CoroutineScope(Dispatchers.Default).launch {
                work()
            }
}