package com.wm.camerafilterapp.ui.howtoplay;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.wm.camerafilterapp.R;

import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;
import com.wm.camerafilterapp.adapter.howToPlayAdapter;
import com.wm.camerafilterapp.databinding.ActivityHowtoplayBinding;
import com.wm.camerafilterapp.model.howtoplaymodel.ResultItem;
import com.wm.camerafilterapp.ui.reportproblem.ReportProblemActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.viewmodel.HowtoplayViewModel;

import java.util.List;

public class HowtoplayActivity extends AppCompatActivity implements View.OnClickListener {
    private ActivityHowtoplayBinding binding;
    howToPlayAdapter howToPlayAdapter;
    ProgressD progressDialog;
    HowtoplayViewModel howtoplayViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_howtoplay);
        binding.backIcon.setOnClickListener(this);
        binding.relativeGetstarted.setOnClickListener(this);
        howtoplayViewModel = new ViewModelProvider(this).get(HowtoplayViewModel.class);

        if(CommonMethod.isOnline(HowtoplayActivity.this))
        {
            getGameRulesApi(LoginPreferences.getActiveInstance(this).getToken());
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), HowtoplayActivity.this);
        }
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.back_icon:
                 finish();
                break;

            case R.id.relative_getstarted:
                Toast.makeText(this, getString(R.string.comingsoon), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void getGameRulesApi(String token)
    {
        progressDialog = ProgressD.show(HowtoplayActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        howtoplayViewModel.Howtoplay(token).observe(this, HowtoPlayResponse ->
        {
            progressDialog.dismiss();
            if(HowtoPlayResponse!=null && HowtoPlayResponse.getCode()==200)
            {
                List<ResultItem> ruleslist= HowtoPlayResponse.getResult();
                binding.recyclerviewPlayRules.setLayoutManager(new LinearLayoutManager(HowtoplayActivity.this, LinearLayoutManager.VERTICAL, false));
                howToPlayAdapter = new howToPlayAdapter(HowtoplayActivity.this,ruleslist);
                binding.recyclerviewPlayRules.setAdapter(howToPlayAdapter);
                binding.linearAssistance.setVisibility(View.VISIBLE);
            }
            else
            {
                Toast.makeText(HowtoplayActivity.this, HowtoPlayResponse.getmessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}