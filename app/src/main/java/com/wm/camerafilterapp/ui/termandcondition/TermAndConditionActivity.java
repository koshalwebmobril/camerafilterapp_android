package com.wm.camerafilterapp.ui.termandcondition;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.wm.camerafilterapp.R;
import com.wm.camerafilterapp.ui.auth.LoginActivity;
import com.wm.camerafilterapp.utils.CommonMethod;
import com.wm.camerafilterapp.utils.ProgressD;
import com.wm.camerafilterapp.utils.UrlApi;

import java.util.List;

public class TermAndConditionActivity extends AppCompatActivity
{
    WebView webView;
    String page_status;
    ImageView img_back;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_and_condition);
        webView=findViewById(R.id.webView);
        img_back=findViewById(R.id.img_back);
        page_status=getIntent().getStringExtra("page_status");

        webView.getSettings().setJavaScriptEnabled(true); // enable javascript
        final Activity activity = this;
        final ProgressD progressDialog = ProgressD.show(TermAndConditionActivity.this,getResources().getString(R.string.logging_in), true, false, null);
        webView.setWebViewClient(new WebViewClient()
        {

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                progressDialog.dismiss();
            }
            @Override
            public void onPageFinished(WebView view, String url)
            {
                super.onPageFinished(view, url);
                progressDialog.dismiss();
            }
        });

        if(CommonMethod.isOnline(TermAndConditionActivity.this))
        {
            webView.loadUrl(UrlApi.BASE_URL+UrlApi.GETPAGE+page_status);
        }
        else
        {
            CommonMethod.showAlert(getString(R.string.check_internet), TermAndConditionActivity.this);
        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




    }
}