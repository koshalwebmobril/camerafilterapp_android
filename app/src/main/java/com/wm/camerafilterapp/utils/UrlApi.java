package com.wm.camerafilterapp.utils;

public class UrlApi
{
     public static String BASE_URL = "http://webmobril.org/dev/camerafilter/api/v1/";    //webmobril server
    public static final String REGISTER="register_user";
    public static final String LOGIN="login_user";
    public static final String VERIFYOTP="verify_otp";
    public static final String FORGOTPASSWORD="forgot_password";
    public static final String RESETPASSWORD="reset_password";
    public static final String CHANGEPASSWORD="change_password";
    public static final String GAMERULE="game_rule";
    public static final String FEEDBACKFORM="feedback";
    public static final String REPORTAPROBLEMAPI="submit_reason";
    public static final String REASON="reason";
    public static final String GETPAGE="get_page?page_id=";
}
