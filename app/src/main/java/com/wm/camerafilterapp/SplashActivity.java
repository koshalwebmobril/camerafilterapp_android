package com.wm.camerafilterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.wm.camerafilterapp.Sharedpreference.LoginPreferences;
import com.wm.camerafilterapp.ui.auth.LoginActivity;
import com.wm.camerafilterapp.ui.surfaceview.CameraSurfaceActivity;

public class SplashActivity extends AppCompatActivity
{
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handler = new Handler();
        handler.postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                String  value= LoginPreferences.getActiveInstance(SplashActivity.this).getToken();
                Log.d("TAG", value);
                if(value.isEmpty() || value.equals(""))
                {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    SplashActivity.this.startActivity(intent);
                    SplashActivity.this.finish();
                }
                else
                {
                    Intent intent = new Intent(SplashActivity.this, CameraSurfaceActivity.class);
                    SplashActivity.this.startActivity(intent);
                    SplashActivity.this.finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        handler.removeCallbacksAndMessages(null);
        finish();
    }
}