package com.wm.camerafilterapp.model.getReportProblemmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class GetIssueListResponse{

	@SerializedName("result")
	private List<ResultItem> result;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public List<ResultItem> getResult(){
		return result;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}