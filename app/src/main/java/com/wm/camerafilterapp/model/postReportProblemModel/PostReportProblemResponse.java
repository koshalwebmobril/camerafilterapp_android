package com.wm.camerafilterapp.model.postReportProblemModel;

import com.google.gson.annotations.SerializedName;

public class PostReportProblemResponse{

	@SerializedName("error")
	private boolean error;

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}


	public int getCode(){
		return code;
	}
}