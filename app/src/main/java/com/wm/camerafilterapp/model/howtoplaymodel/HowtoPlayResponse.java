package com.wm.camerafilterapp.model.howtoplaymodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class HowtoPlayResponse{

	@SerializedName("result")
	private List<ResultItem> result;

	@SerializedName("code")
	private int code;

	@SerializedName("message")
	private String message;

	@SerializedName("error")
	private boolean error;

	public List<ResultItem> getResult(){
		return result;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getmessage(){
		return message;
	}
}