package com.wm.camerafilterapp.model.changepasswordmodel;

import com.google.gson.annotations.SerializedName;

public class ChangepasswordResponse{

	@SerializedName("result")
	private Result result;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public Result getResult(){
		return result;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}