package com.wm.camerafilterapp.model.changepasswordmodel;

import com.google.gson.annotations.SerializedName;

public class Result{

	@SerializedName("address")
	private Object address;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("verified")
	private int verified;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("otp")
	private int otp;

	@SerializedName("email_verified_at")
	private Object emailVerifiedAt;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("type")
	private int type;

	@SerializedName("is_admin")
	private int isAdmin;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("name")
	private Object name;

	@SerializedName("id")
	private int id;

	@SerializedName("email")
	private String email;

	@SerializedName("status")
	private int status;

	public Object getAddress(){
		return address;
	}

	public String getMobile(){
		return mobile;
	}

	public int getVerified(){
		return verified;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getOtp(){
		return otp;
	}

	public Object getEmailVerifiedAt(){
		return emailVerifiedAt;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public int getType(){
		return type;
	}

	public int getIsAdmin(){
		return isAdmin;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public Object getName(){
		return name;
	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public int getStatus(){
		return status;
	}
}