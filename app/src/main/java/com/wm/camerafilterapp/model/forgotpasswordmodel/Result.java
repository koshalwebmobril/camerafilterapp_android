package com.wm.camerafilterapp.model.forgotpasswordmodel;

import com.google.gson.annotations.SerializedName;

public class Result{

	@SerializedName("otp")
	private int otp;

	public int getOtp(){
		return otp;
	}
}