package com.wm.camerafilterapp.model.filterimagenamemodel;

public class FilterImageModel
{
        private int id;
        private String name;
        private int image;
        private int status;

    public int getStatus()
    {
        return status;
    }

    public void setStatus(int status)
    {
        this.status = status;
    }

    public FilterImageModel(int image, String name, int id, int status)
    {
        this.image=image;
        this.name=name;
        this.id=id;
        this.status=status;
    }

        public int getId()
        {
            return id;
        }
        public void setId(int id)
        {
            this.id = id;
        }
        public String getName()
        {
            return name;
        }
        public void setName(String name)
        {
            this.name =name;
        }


    public int getImage()
    {
        return image;
    }
    public void setImage(int image)
    {
        this.image =image;
    }

}
