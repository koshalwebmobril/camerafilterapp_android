package com.wm.camerafilterapp.model.verificationmodel;

import com.google.gson.annotations.SerializedName;

public class VerificationResponse {

	@SerializedName("result")
	private Result result;

	@SerializedName("code")
	private int code;

	@SerializedName("error")
	private boolean error;

	@SerializedName("message")
	private String message;

	public Result getResult(){
		return result;
	}

	public int getCode(){
		return code;
	}

	public boolean isError(){
		return error;
	}

	public String getMessage(){
		return message;
	}
}